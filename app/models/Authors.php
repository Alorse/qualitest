<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Authors extends Model
{
    protected $table = 'authors';
    protected $fillable = ['name', 'email', 'nationality_id'];
    protected $hidden = ['created_at', 'updated_at'];
    public $timestamps = true;

    public function nationality()
    {
        return $this->belongsTo('App\models\Nationalities','nationality_id');
    }
}
