<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $table = 'books';
    protected $fillable = ['title', 'description', 'image', 'published', 'author_id'];
    protected $hidden = ['created_at', 'updated_at'];
    public $timestamps = true;

    public function author()
    {
        return $this->belongsTo('App\models\Authors','author_id');
    }
}
