<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Nationalities extends Model
{
    protected $table = 'nationalities';
    protected $fillable = ['country', 'abbreviation', 'adjective', 'person'];
    public $timestamps = false;
}
