<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

// API
Route::group(['prefix' => 'api'], function()
{   
    // Nationalities
 	Route::get('nationalities', 'qualit\NationalitiesController@index');
	Route::get('nationalities/{id}', 'qualit\NationalitiesController@show');
	// Authors
	Route::get('authors/{skip}/{limit}', 'qualit\AuthorsController@index');
	Route::post('authors', 'qualit\AuthorsController@store');
	Route::get('authors/{id}', 'qualit\AuthorsController@show');
	Route::post('authors/{id}', 'qualit\AuthorsController@update');
	Route::delete('authors/{id}', 'qualit\AuthorsController@destroy');
	// Books
	Route::get('books/{skip}/{limit}', 'qualit\BooksController@index');
	Route::post('books', 'qualit\BooksController@store');
	Route::get('books/{id}', 'qualit\BooksController@show');
	Route::post('books/{id}', 'qualit\BooksController@update');
	Route::delete('books/{id}', 'qualit\BooksController@destroy');
});
