<?php

namespace App\Http\Controllers\qualit;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\Books;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($skip = 0, $limit = 20)
    {
        $count = Books::count();
        $books = Books::with('author')->skip($skip)->take($limit)->get();
        return response()->json([
            'msg'   => 'success',
            'count' => $count,
            'books' => $books
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'title'         => 'required|max:255',
            'description'   => 'required',
            'published'     => 'date',
            'author_id'     => 'required'
        ]);

        $data = $request->all();
        $newBook = Books::create([
            'title'         => $data['title'],
            'description'   => $data['description'],
            'image'         => isset($data['image']) ? $data['image']: '',
            'published'     => $data['published'],
            'author_id'     => $data['author_id']
        ]);

        return response()->json([
            'msg'   => 'success',
            'book'  => $newBook->toArray()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $book = Books::find($id);
        $book = Books::with('author')->where('id', $id)->get();
        if($book){
            $success = true;
            $msg = "successfully retrived";
        }else{
            $success = false;
            $msg = "There was a error, please try again later";
        }
        return response()->json([
            'success'   => $success,
            'msg'       => $msg,
            'book'      => $book->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $this->validate($request, [
            'title'         => 'required|max:255',
            'description'   => 'required',
            'published'     => 'date',
            'author_id'     => 'required'
        ]);

        $data = $request->all();
        $book = Books::find($id);
        $book->title = $data["title"];
        $book->description = $data["description"];
        $book->image = isset($data['image']) ? $data['image'] : $book->image;
        $book->published = $data["published"];
        $book->author_id = $data["author_id"];

        $book->update();

        return response()->json([
            'msg'   => 'success',
            'book'  => $book->toArray()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Books::find($id);
        $book->delete();
        return response()->json([
            'msg'       => $book != null ? 'success' : 'not_found'
        ]);
    }
}
