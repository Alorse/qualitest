<?php

namespace App\Http\Controllers\qualit;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\Authors;

class AuthorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($skip = 0, $limit = 20)
    {
        $count = Authors::count();
        $authors = ($limit == 0) ? Authors::all() : Authors::with('nationality')->skip($skip)->take($limit)->get();
        return response()->json([
            'msg'   => 'success',
            'count' => $count,
            'authors' => $authors
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'name'      => 'required|max:255',
            'email'     =>  'max:255',
            'nationality_id'     =>  'required'
        ]);

        $data = $request->all();
        $newAuthor = Authors::create([
            'name'  => $data['name'],
            'email' => isset($data['email']) ? $data['email'] : '',
            'nationality_id'    => $data['nationality_id']
        ]);

        return response()->json([
            'msg'   => 'success',
            'author'  => $newAuthor->toArray()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $author = Authors::find($id);
        $author = Authors::with('nationality')->where('id', $id)->get();
        if($author){
            $success = true;
            $msg = "successfully retrived";
        }else{
            $success = false;
            $msg = "There was a error, please try again later";
        }
        return response()->json([
            'success'   => $success,
            'msg'       => $msg,
            'author'    => $author->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $this->validate($request, [
            'name'      => 'required|max:255',
            'email'     =>  'max:255',
            'nationality_id'     =>  'required'
        ]);

        $data = $request->all();
        $author = Authors::find($id);
        $author->name = $data["name"];
        $author->email = $data["email"];
        $author->nationality_id = $data["nationality_id"];

        $author->update();

        return response()->json([
            'msg'   => 'success',
            'author'  => $author->toArray()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $author = Authors::find($id);
        $author->delete();
        return response()->json([
            'msg'       => $author != null ? 'success' : 'not_found'
        ]);
    }
}
