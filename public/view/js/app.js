function app(){
	var _self = this;
	this.$API = 'http://localhost/qualitest/public/';
	this.$URI = _self.$API + 'view/'; // Server URL
	
	// Constructor
	this.index = function(){
		var MyApp = angular.module('qtapp', ['ngRoute', 'ngMaterial', 'ngMessages']);
		_controllers.get(MyApp);
	};

	/*
		Convert image to base64 code
	 */
	this.imgToBase64 = function($scope, id){
		var handleFileSelect = function(evt) {
		    var files = evt.target.files;
		    var file = files[0];

		    if (files && file) {
		        var reader = new FileReader();
		        reader.onload = function(readerEvt) {
		            var binaryString = readerEvt.target.result;
		            $scope.image = 'data:image/jpeg;base64,'+ btoa(binaryString);
		        };
		        reader.readAsBinaryString(file);
		    }
		};
		document.getElementById(id).addEventListener('change', handleFileSelect, false);
	};

	/*
	 * Global controller
	 */

	this.appCtrl = ['$scope', '$http', '$log', function($scope, $http, $log){
		$scope.config = {
			URLs: {
				$URIAuth: _self.$URIAuth,
				$URI: _self.$URI,
				$PATH: _self.$PATH,
			}
		};
		$scope.books = {};
		$scope.authors = {};

		$scope.loadInfoBook = function(id) {
			$http.get(_self.$API + 'api/books/' + id)
			.then(function(data){
				$scope.books.current = data.data.book[0];
				$('#infoBook').modal();
			},function(error) {
				$log.error(error);
			});
		};
		
		$scope.addItem = function(where, value){
			where.push(value);
		};

		var randomParameter = Math.floor((Math.random()*999)+1); //Temporal o solo para desarrollo, en producción el valor de esta variable debe ser estatico.
		$scope.urls = {};
		$scope.urls.overall_header = 'views/overall_header.html?random=' + randomParameter;
		$scope.urls.overall_footer = 'views/overall_footer.html?random=' + randomParameter;
		$scope.urls.book_view = 'views/book_view.html?random=' + randomParameter;
	}];

	/*
	 * Home controller
	 */

	this.homeCtrl = ['$scope', '$route', '$http' ,function($scope, $route, $http){
		$http.get(_self.$API + 'api/books/0/10')
		.then(function(data){
			$scope.books.last = data.data.books;
		},function(error) {
			console.log(error);
		});
	}];
}

var _controllers = new controllers(); _controllers._init();
var books = new books(); books._init();
var authors = new authors(); authors._init();
var main = new app(); main.index();