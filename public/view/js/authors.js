function authors(){
	
	var _self = this;
	this.$PATH = '#/';
	/**
	 * [_init Constructor por defecto]
	 * @return {[type]} [description]
	 */
	this._init = function(){
		// console.log('Constructor authors Class');
	};

	/*
	List the authors in frontend
	 */
	this.list = function($scope, $http, $route){
		var skip = 0;
		var limit = 10;
		$scope.currentpage = 1;
		$scope.getListAuthors = function(skip, limit){
			$scope.pages = 1;
			$http.get(main.$API + 'api/authors/' + skip + '/'+ limit)
			.then(function(data){
				$scope.authors.all = data.data.authors;
				$scope.authors.count = data.data.count;
				if($scope.authors.count%limit == 0){
					$scope.page = parseInt($scope.authors.count/limit);
				} else{
					$scope.page = parseInt($scope.authors.count/limit) + 1;
				}
				$scope.pages = [];
				for (var i = 0; i <= $scope.page-1; i++) {
					$scope.pages[i] = i;
				}
			},function(error) {
				console.log(error);
			});
		};

		$scope.jumpPage = function(page){
			var skip = (page -1) * limit;
			$scope.currentpage = page;
			$scope.getListAuthors(skip, limit);
		};
		$scope.getListAuthors(skip, limit);

		$scope.authorDelete = function(id, name){
			bootbox.confirm('Are you sure to delete the author "' + name + '"?', function(result) {
				if(result){
					$http.delete(main.$API + 'api/authors/'+ id)
					.then(function(data){
						$route.reload();
						bootbox.alert("Author delete successfully!", function() {
							console.log(data);
						});
					},function(error) {
						$log.error(error);
					});
				}
			}); 
		};
	};
	/*
	Add new author in database
	 */
	this.add = function($scope, $http, $timeout, $log, $location, $route){
		_self.autoComplete($scope, $log, $http);
		$scope.saveAuthor = function(){
			$scope.author = {
				name: $scope.name,
				email: $scope.email,
				nationality_id: $scope.nationality
			};

			$http.post(main.$API + 'api/authors', $scope.author)
			.then(function(data){
				bootbox.alert("Author saved successfully!", function() {
					$location.path('/authorlist');
					$route.reload();
				});
			},function(error) {
				console.log(error);
				var log = [];
				$scope.error = [];
				angular.forEach(error.data, function(value, key) {
					$scope.addItem($scope.error, value[0]);
				}, log);
				bootbox.alert($scope.error[0]);
			});
		};
	};

	/*
		Update author in database
	 */

	this.update = function($scope, $http, $location, $route, $timeout, $log, $routeParams){
		$http.get(main.$API + 'api/authors/' + $routeParams.id)
		.then(function(data){
			$scope.name = data.data.author[0].name;
			$scope.email = data.data.author[0].email;
			$scope.selectedItem = data.data.author[0].nationality
			_self.autoComplete($scope, $log, $http);

			$scope.saveAuthor = function(){
				$scope.author = {
					name: $scope.name,
					email: $scope.email,
					nationality_id: $scope.nationality
				};

				$http.post(main.$API + 'api/authors/' + $routeParams.id,$scope.author)
				.then(function(data){
					bootbox.alert("Author update successfully!", function() {
						$location.path('/authorlist');
						$route.reload();
					});
				},function(error) {
					var log = [];
					$scope.error = [];
					angular.forEach(error.data, function(value, key) {
						$scope.addItem($scope.error, value[0]);
					}, log);
					bootbox.alert($scope.error[0]);
				});
			};
		},function(error) {
			$log.error(error);
		});
	};

	/*
		Function autocomplete for nationalities
	 */

	this.autoComplete = function($scope, $log, $http){
		loadAll();
		$scope.querySearch   = querySearch;
		$scope.selectedItemChange = selectedItemChange;
		function querySearch (query) {
			var results = query ? $scope.nationalities.filter( createFilterFor(query) ) : $scope.nationalities, deferred;
			return results;
		}
		function selectedItemChange(item) {
			$log.info('Item changed to ' + JSON.stringify(item));
			$scope.nationality = item.id
		}
		function loadAll() {
			$http.get(main.$API + 'api/nationalities')
			.then(function(data){
				$scope.nationalities = data.data.nationalities;
			},function(error) {
				$log.error(error);
			});
		}
		function createFilterFor(query) {
			var lowerQuery = query.toLowerCase();
			return function filterFn(state) {
				return (state.adjective.toLowerCase().indexOf(lowerQuery) === 0);
			};
		}
	};
}