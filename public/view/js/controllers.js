function controllers(){
	
	var _self = this;
	this.$PATH = '#/';
	/**
	 * [_init Constructor por defecto]
	 * @return {[type]} [description]
	 */
	this._init = function(){
		// console.log('Constructor controllers Class');
	};

	/**
	* [get Configura los controladores de AngularJS]
	* @param  [ctrl]  [Indetificador del App]
	*/
	this.get = function(ctrl){
		ctrl.config(function($routeProvider) {
			$routeProvider
			.when('/', {
				templateUrl: 'views/home.html',
				controller: main.homeCtrl
			})
			.when('/booklist', {
				templateUrl: 'views/book_list.html',
				controller: 'booklistController'
			})
			.when('/booknew', {
				templateUrl: 'views/book_new.html',
				controller: 'booknewController'
			})
			.when('/bookedit/:id', {
				templateUrl: 'views/book_new.html',
				controller: 'bookeditController'
			})
			.when('/authorlist', {
				templateUrl: 'views/author_list.html',
				controller: 'authorlistController'
			})
			.when('/authornew', {
				templateUrl: 'views/author_new.html',
				controller: 'authornewController'
			})
			.when('/authoredit/:id', {
				templateUrl: 'views/author_new.html',
				controller: 'authoreditController'
			})
			.otherwise({
				redirectTo: '/'
			});
		});

		/*
		 * Controllers for Books
		 */
		ctrl.controller('booklistController', ['$scope', '$http', '$route', function($scope, $http, $route){
			books.list($scope, $http, $route);
		}]);

		ctrl.controller('booknewController', ['$scope', '$http', '$location', '$route', '$timeout', '$log', function($scope, $http, $location, $route, $timeout, $log){
			books.add($scope, $http, $location, $route, $timeout, $log);
		}]);

		ctrl.controller('bookeditController', ['$scope', '$http', '$location', '$route', '$timeout', '$log', '$routeParams', function($scope, $http, $location, $route, $timeout, $log, $routeParams){
			books.update($scope, $http, $location, $route, $timeout, $log, $routeParams);
		}]);

		/*
		 * Controllers for Authors
		 */
		ctrl.controller('authorlistController', ['$scope', '$http', '$route', function($scope, $http, $route){
			authors.list($scope, $http, $route);
		}]);

		ctrl.controller('authornewController', ['$scope', '$http', '$timeout', '$log', '$location', '$route' , function($scope, $http, $timeout, $log, $location, $route){
			authors.add($scope, $http, $timeout, $log, $location, $route);
		}]);

		ctrl.controller('authoreditController', ['$scope', '$http', '$location', '$route', '$timeout', '$log', '$routeParams', function($scope, $http, $location, $route, $timeout, $log, $routeParams){
			authors.update($scope, $http, $location, $route, $timeout, $log, $routeParams);
		}]);

		ctrl.controller('qtappCtrl', main.appCtrl);
	};
}