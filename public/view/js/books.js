function books(){
	var _self = this;
	this.$PATH = '#/';
	/**
	 * [_init Constructor por defecto]
	 * @return {[type]} [description]
	 */
	this._init = function(){
		// console.log('Constructor books Class');
	};

	/*
		List de books in frontend
	 */
	this.list = function($scope, $http, $route){
		var skip = 0;
		var limit = 10;
		$scope.currentpage = 1;
		$scope.getListBooks = function(skip, limit){
			$scope.pages = 1;
			$http.get(main.$API + 'api/books/' + skip + '/'+ limit)
			.then(function(data){
				$scope.books.all = data.data.books;
				$scope.books.count = data.data.count;
				if($scope.books.count%limit == 0){
					$scope.page = parseInt($scope.books.count/limit);
				} else{
					$scope.page = parseInt($scope.books.count/limit) + 1;
				}
				$scope.pages = [];
				for (var i = 0; i <= $scope.page-1; i++) {
					$scope.pages[i] = i;
				}
			},function(error) {
				console.log(error);
			});
		};

		$scope.jumpPage = function(page){
			var skip = (page -1) * limit;
			$scope.currentpage = page;
			$scope.getListBooks(skip, limit);
		};
		$scope.getListBooks(skip, limit);

		$scope.bookDelete = function(id, name){
			bootbox.confirm('Are you sure to delete the book "' + name + '"?', function(result) {
				if(result){
					$http.delete(main.$API + 'api/books/'+ id)
					.then(function(data){
						$route.reload();
						bootbox.alert("Book delete successfully!", function() {
							console.log(data);
						});
					},function(error) {
						$log.error(error);
					});
				}
			}); 
		};
	};

	/*
		Add new book un database
	 */
	this.add = function($scope, $http, $location, $route, $timeout, $log){
		$scope.published = new Date();
		$scope.maxDate = new Date(
			$scope.published.getFullYear(),
			$scope.published.getMonth(),
			$scope.published.getDate()
		);
		_self.autoComplete($scope, $timeout, $log, $http);
		main.imgToBase64($scope, 'image');

		$scope.saveBook = function(){
			$scope.book = {
				title: $scope.title,
				description: $scope.description,
				published: $scope.published,
				image: $scope.image,
				author_id: $scope.author
			};

			$http.post(main.$API + 'api/books',$scope.book)
			.then(function(data){
				bootbox.alert("Book saved successfully!", function() {
					$location.path('/booklist');
					$route.reload();
				});
			},function(error) {
				console.log(error);
				var log = [];
				$scope.error = [];
				angular.forEach(error.data, function(value, key) {
					$scope.addItem($scope.error, value[0]);
				}, log);
				bootbox.alert($scope.error[0]);
			});
		};
	};

	/*
		Udapate the book in database
	 */
	this.update = function($scope, $http, $location, $route, $timeout, $log, $routeParams){
		$http.get(main.$API + 'api/books/' + $routeParams.id)
		.then(function(data){
			$scope.title = data.data.book[0].title;
			$scope.description = data.data.book[0].description;
			$scope.published = new Date(data.data.book[0].published);
			$scope.selectedItem = data.data.book[0].author
			_self.autoComplete($scope, $timeout, $log, $http);
			main.imgToBase64($scope, 'image');

			$scope.maxDate = new Date(
				new Date().getFullYear(),
				new Date().getMonth(),
				new Date().getDate()
			);

			$scope.saveBook = function(){
				$scope.book = {
					title: $scope.title,
					description: $scope.description,
					published: $scope.published,
					image: $scope.image,
					author_id: $scope.author
				};

				$http.post(main.$API + 'api/books/' + $routeParams.id,$scope.book)
				.then(function(data){
					bootbox.alert("Book update successfully!", function() {
						$location.path('/booklist');
						$route.reload();
					});
				},function(error) {
					var log = [];
					$scope.error = [];
					angular.forEach(error.data, function(value, key) {
						$scope.addItem($scope.error, value[0]);
					}, log);
					bootbox.alert($scope.error[0]);
				});
			};
		},function(error) {
			$log.error(error);
		});
	};

	/*
		Function autocomplete for authors
	 */
	this.autoComplete = function($scope, $timeout, $log, $http){
		loadAll();
		$scope.querySearch   = querySearch;
		$scope.selectedItemChange = selectedItemChange;
		function querySearch (query) {
			var results = query ? $scope.authors.filter( createFilterFor(query) ) : $scope.authors, deferred;
			return results;
		}
		function selectedItemChange(item) {
			$log.info('Item changed to ' + JSON.stringify(item));
			$scope.author = item.id;
		}
		function loadAll() {
			$http.get(main.$API + 'api/authors/0/0') //Second 0 is unlimited
			.then(function(data){
				$scope.authors = data.data.authors;
			},function(error) {
				$log.error(error);
			});
		}
		function createFilterFor(query) {
			var lowerQuery = query.toLowerCase();
			return function filterFn(state) {
				return (state.adjective.toLowerCase().indexOf(lowerQuery) === 0);
			};
		}
	};
};